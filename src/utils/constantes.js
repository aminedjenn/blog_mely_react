export const cloudURI = "https://blob-api.herokuapp.com/api/";
export const localURI = "http://localhost:3001/api/";
export const URI = process.env.NODE_ENV === "production" ? cloudURI : localURI;
