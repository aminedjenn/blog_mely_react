import styled from "@emotion/styled";
import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Lottie from "react-lottie";
import animationData from "../lotties/67782-error";
import Alert from "react-bootstrap/Alert";

export function ScrollToTop() {
  const { pathname } = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

export const UnderTitle = styled.div`
  width: 200px;
  background: linear-gradient(
    170deg,
    rgba(102, 69, 249, 1) 25%,
    rgba(224, 130, 255, 0.8687850140056023) 80%
  );
  height: 4px;
  margin-top: 18px;
  border-radius: 2px;
`;

export const PostLine = styled.div`
  margin: 0 auto;
  width: 70%;
  height: 1px;
  margin-top: 10px;
  margin-bottom: 50px;
  background-color: rgb(150 150 150 / 72%);
`;

export const tagsDetail = {
  plante: {
    class: "dark",
    text: "plante",
  },
  lune: {
    class: "dark",
    text: "lune",
  },
  astrologie: {
    class: "dark",
    text: "astrologie",
  },
  lithoterapie: {
    class: "dark",
    text: "lithoterapie",
  },
};
export const SpanCheck = styled.span`
  padding: 15px 22px;
  border-radius: 50px;
  background: #1eaf44;
  margin-left: 12px;
`;

export const MyVerticallyCenteredModal = (props) => {
  const { content } = props;
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <div className="font-weight-bold text-center ml-2 mt-5 mb-5">
          {content}{" "}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>
          OK
          {/* <StyledLink to="/blog/">OK</StyledLink>{" "} */}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export const CustomAlert = (props) => {
  const { message, show, setShow } = props;
  return (
    <Alert
      show={show}
      variant="danger"
      onClose={() => setShow(false)}
      dismissible
    >
      <div className="d-flex align-items-baseline">
        <div className="svg">
          <Lottie options={defaultOptions} height={80} width={80} />
        </div>
        <p className="ml-2 text-center">
          <span className="font-weight-bold">Oh! Erreur -</span> {message}
        </p>
      </div>
    </Alert>
  );
};

const StyledNoPost = styled.div`
  padding: 50px;
  margin: 50px;
  background: white;
  border: 1px solid #e0e0e0;
  border-radius: 15px;
`;

export const NoPost = () => {
  return (
    <StyledNoPost className="d-flex justify-content-center shadow-sm">
      Oh! Il n'y a pas encore de post,
      <Link className="mr-1 ml-1" to="/post/add">
        En crée un,
      </Link>
      <Link to="/connexion">ou connecte toi</Link>
    </StyledNoPost>
  );
};
