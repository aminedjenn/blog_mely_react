import axios from "axios";
import * as CONST from "../../utils/constantes";
import { getRequest, postRequest } from "../axios";

export const loginBack = async (option) => {
  const { data, request } = option;
  const res = await axios({
    method: "POST",
    url: `${CONST.URI}${request}`,
    data: data,
  });
  return res;
};
