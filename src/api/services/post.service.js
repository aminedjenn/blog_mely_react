import axios from "axios";
import * as CONST from "../../utils/constantes";
import { getRequest, postRequest } from "../axios";

export const uploadFile = async (option) => {
  const { data } = option;

  const config = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  return axios
    .post(`${CONST.URI}file/uploadImagePost`, data, { headers: config })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const deletePostApi = async (option) => {
  const { query, header } = option;

  axios
    .delete(`${CONST.URI}${query}`, { headers: header })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const postTurtle = async (option) => {
  const { id, data } = option;
  return axios({
    method: "POST",
    url: `${CONST.URI}posts/like/${id}`,
    data: { data },
  })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getLastsPost = async (req) => {
  return getRequest({ request: req })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return console.error(error);
    });
};
