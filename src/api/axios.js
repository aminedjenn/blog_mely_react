import axios from "axios";
import * as CONST from "../utils/constantes";
/**
 *
 * @param {object} option {option.data} for the data parameters.
 * @return {promise} erreur ou pas.
 * @example getRequest({ query : "/my/exemple?param="dfdfg"&test="encore""})
 */
export const getRequest = async (option) => {
  let query = option.query === undefined || null ? { "": "" } : option.query;
  const qu = Object.entries(query)
    .map((q) => {
      return `&${[...q][0]}=${[...q][1]}`;
    })
    .reduce((acc, curr) => {
      return acc + curr;
    }, [])
    .replace(/^&=/g, "");
  return await axios({
    method: "GET",
    url: `${CONST.URI}${option.request}?${qu}`,
  });
};

/**
 *
 * @param {object} option {option.data} for the data parameters.
 * @return {string} erreur ou pas.
 * @example postRequest({ request : "/my/exemple", data: {test : "test", other: "other"}})
 */
export const postRequest = async (option) => {
  const { data, request, headers } = option;

  axios({
    method: "POST",
    url: `${CONST.URI}${request}`,
    data: data,
    headers: headers,
  })
    .then((res) => {
      return console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
