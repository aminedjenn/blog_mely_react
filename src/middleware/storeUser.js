import Cookies from "universal-cookie";

const storeUser = (data) => {
  const cookies = new Cookies();
  cookies.set("token", data.token);
  localStorage.setItem("isLogin", true);
};

export default storeUser;
