import "./App.css";
import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Header from "./componants/header/header";
import Main from "./componants/home/main";
import Disposition from "./componants/post/Disposition";
import AddPost from "./componants/editor/addPost";
import AuthForm from "./componants/auth/authForm";
import Logout from "./componants/auth/logout";
import DetailPost from "./componants/post/detailPost";
import BlogHeaderContent from "./componants/post/BlogHeaderContent";
import { ParallaxProvider } from "react-scroll-parallax";
import { ScrollToTop } from "./utils/utils";
import { AuthContext } from "./componants/auth/authContext";
import { SwitchContext } from "./componants/header/switchContext";
import AOS from "aos";
import "aos/dist/aos.css";

function App() {
  const [deleteMode, setDeleteMode] = useState(false);
  const defaultContext = {
    deleteMode,
    setDeleteMode,
  };
  const [open, setOpen] = useState(false);
  const [user, setUser] = useState();
  const [token, setToken] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const state = () => {
    setOpen(!open);
  };

  const contextValue = {
    isLoggedIn,
    user: user,
    token,
    login: setUser,
    setToken,
    setIsLoggedIn,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      <SwitchContext.Provider value={defaultContext}>
        <Router>
          <ScrollToTop />
          <ParallaxProvider>
            <Header onClick={state} open={open} name="Melythologie" />
            <div
              id="all"
              style={{ height: "90vh" }}
              onClick={() => {
                setOpen(false);
              }}
            >
              <Switch>
                <Route exact path="/">
                  <BlogHeaderContent />
                  <Main className="pb-5 h-100"></Main>
                </Route>
                <Route exact path="/blog" component={Disposition} />
                <Route exact path="/blog/:filter" component={Disposition} />
                <Route path="/blog/post/:id" component={DetailPost} />
                <Route exact path="/post/add" component={AddPost} />
                <Route exact path="/file/upload" component={AddPost} />
                <Route exact path="/connexion">
                  {contextValue.isLoggedIn ? <Redirect to="/" /> : <AuthForm />}
                </Route>
                <Route exact path="/logout">
                  <Logout />
                  <Redirect to="/" />
                </Route>
              </Switch>
            </div>
          </ParallaxProvider>
        </Router>
      </SwitchContext.Provider>
    </AuthContext.Provider>
  );
}

AOS.init({
  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  duration: 700, // values from 0 to 3000, with step 50ms
  easing: "ease-in-out", // default easing for AOS animations
  once: false, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  startEvent: "load",
});

export default App;
