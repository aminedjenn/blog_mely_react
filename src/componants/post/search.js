import styled from "@emotion/styled";
import React from "react";
import { getRequest } from "../../api/axios";

const Input = styled.input`
  width: 100%;
  outline: none;
  padding: 20px 36px;
  border: none;
  box-shadow: 1px 3px 60px 0px #bbbbbb4d;
  caret-color: currentcolor;
  border-radius: 30px;
  border: 1px solid #c1c1c12b;
  bottom: -25px;
  right: 0;
  transition: all 0.4s ease;
  &:focus {
    box-shadow: 0px 12px 60px 0px #a0a0a054;
  }
  &:hover {
    box-shadow: 0px 12px 60px 0px #a0a0a054;
  }
`;

const ButtonSearch = styled.button`
  outline: none;
  position: absolute;
  padding: 18px 22px;
  margin: 0;
  right: 3px;
  border: none;
  color: black;
  background: transparent;
  border-radius: 30px;
  bottom: 3px;
  transition: all 0.3s;
  &:hover {
    background: #4294b338;
  }
`;

const ContainerSearch = styled.div`
  width: 100%;
  align-self: center;
  position: relative;
  flex: 1 1 50%;
`;

const WrapperSearch = styled.form`
  position: relative;
  flex: 1 1 50%;
  width: 100%;
  min-width: 250px;
}
`;

export const SearchInput = (props) => {
  const handleSubmit = (e) => {
    const searchQuery = e.target.search.value;

    e.preventDefault();
    if (searchQuery.length <= 0) {
      return props.parentCallback({ search: {}, hasMore: true });
    }
    getRequest({
      request: "posts/search",
      query: { search: searchQuery },
    })
      .then((res) => {
        props.parentCallback({ search: res.data.data, hasMore: false });
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <ContainerSearch>
      <WrapperSearch onSubmit={handleSubmit}>
        <Input
          type="text"
          name="search"
          placeholder="Rechercher un article"
          size="30"
        />
        <ButtonSearch type="submit">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="black"
            className="bi bi-search"
            viewBox="0 0 16 16"
          >
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
          </svg>
        </ButtonSearch>
      </WrapperSearch>
    </ContainerSearch>
  );
};
