import React, { useState } from "react";
import { BsFillGridFill, BsGrid3X3GapFill } from "react-icons/bs";
import { HiMenu } from "react-icons/hi";
import styled from "@emotion/styled";
import Posts from "./posts";

const Dispo = styled.button`
  padding: 2px 6px;
  background: #e6e6e6;
  border: none;
  margin: 26px 3px;
  border-radius: 5px;
  box-shadow: 2px 2px 20px -2px #9090902b;
  transition: 0.3s all ease;
  &:hover {
    background: rgb(212, 212, 212);
  }
`;

const ContainerDispo = styled.div`
  right: 10%;
  top: 120px;
  position: absolute;
`;

const Disposition = () => {
  const [disposition, setDisposition] = useState(6);
  const dispoValue = [
    { col: 12, text: <HiMenu fill="#777777" /> },
    { col: 6, text: <BsFillGridFill fill="#777777" /> },
    { col: 4, text: <BsGrid3X3GapFill fill="#777777" /> },
  ];
  return (
    <>
      <ContainerDispo className="d-flex justify-content-start">
        {dispoValue.map((dispo) => {
          return (
            <div key={dispo.col}>
              <Dispo
                className={`${
                  dispo.col === disposition ? "dispo-active" : ""
                } `}
                onClick={() => {
                  setDisposition(dispo.col);
                }}
              >
                {dispo.text}
              </Dispo>
            </div>
          );
        })}
      </ContainerDispo>
      <Posts disposition={disposition} />
    </>
  );
};

export default Disposition;
