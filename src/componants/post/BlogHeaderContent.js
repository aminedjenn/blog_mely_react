import React from "react";
import styled from "@emotion/styled";
import { Parallax, ParallaxBanner } from "react-scroll-parallax";

const StyledTextHeader = styled.div`
  position: absolute;
  top: 50%;
  left: 25%;
  width: 70%;
  z-index: 999;
  color: white;
  font-size: 3.3em;
`;

const BlogHeader = styled.div`
  height: 100vh;
  width: 100%;
  background-color: #93e1d8;
`;

const BlogHeaderContent = (props) => {
  const { children } = props;
  return (
    <BlogHeader>
      {children}
      <ParallaxBanner
        className=""
        layers={[
          {
            image: "/img/lune.jpg",
            amount: 0.6,
          },
        ]}
        style={{
          height: "100%",
          backgroundSize: "auto",
        }}
      >
        <StyledTextHeader data-aos="fade-right" style={{ zIndex: "auto" }}>
          <Parallax y={[-50, 0]} tagOuter="div">
            <p className="w-50 text-white"> Voici mon site trop coule</p>
          </Parallax>
        </StyledTextHeader>
      </ParallaxBanner>
    </BlogHeader>
  );
};

export default BlogHeaderContent;
