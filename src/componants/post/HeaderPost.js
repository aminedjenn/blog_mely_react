import React from "react";
import styled from "@emotion/styled";
import { StyledText } from "../home/main";
import { Image, CloudinaryContext } from "cloudinary-react";

const StyledHeader = styled.div`
  width: 100%;
  display: flex;
  flex: 1 1 0;
  background: linear-gradient(
    90deg,
    rgb(255 255 255) 50%,
    rgb(208 255 255) 50%
  );
  height: 100vh;
  z-index: 1;
  align-items: center;
  justify-content: center;
  & div.metadata > h2 {
    font-size: 3em;
  }
  & metadata {
    min-width: 40%;
  }
`;

const DressedBackground = styled.div`
  position: absolute;
  background-image: radial-gradient(circle, #7f70fb 6%, transparent 6%);
  -webkit-background-size: 50px 50px;
  background-size: 42px 42px;
  width: 100%;
  height: 100%;
  z-index: 2;
  bottom: -11%;
  left: 17%;
`;

const StyledImage = styled(Image)`
  height: auto;
  max-width: 600px;
  max-height: 600px;
  width: 100%;
  border-radius: 15px;
  z-index: 3;
`;

const Title = styled(StyledText)`
  margin: 0;
`;

const HeaderPost = (props) => {
  const { img, metadata } = props;
  return (
    <StyledHeader>
      <div className="img">
        <CloudinaryContext
          className="d-flex justify-content-center position-relative"
          cloudName="dvbzs4nlk"
        >
          <StyledImage src={img} crop="scale" />
          <DressedBackground></DressedBackground>
        </CloudinaryContext>
      </div>
      <div className="metadata p-3 ml-2 w-50">
        <Title className="display-4 font-weight-bold text-capitalize">
          {metadata.titre}
        </Title>
        <p className="m-0">
          By <span className="font-weight-bold">{metadata.author}</span>
        </p>
        <p>Write the {metadata.date}</p>
      </div>
    </StyledHeader>
  );
};

export default HeaderPost;
