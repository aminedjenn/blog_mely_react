import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Post from "./post";
import Filter from "./filter";
import { getRequest } from "../../api/axios";
import { useParams } from "react-router-dom";
import LoadingComponant from "../load/loading";
import { SearchInput } from "./search";
import InfiniteScroll from "react-infinite-scroll-component";

const StyledCol = styled(Col)`
  min-width: 250px;
  & > div.position-relative > img:hover {
    transform: scale(1.8);
  }
`;

const EmptyPost = styled.h1`
  font-weight: bold;
  margin-top: 50px;
  text-align: center;
`;

const StyledRow = styled(Row)`
  height: 100%;
  width: 100%;
  padding-bottom: 200px;
  margin: 0;
`;

const StyledTitle = styled.div`
  padding: 20px;
  & > h1 {
    flex: 1 1 50%;
    background: linear-gradient(
      90deg,
      rgb(0 187 255) 12%,
      rgb(199 140 255 / 95%) 36%,
      rgba(255, 79, 165, 1) 76%
    );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    font-weight: bold;
  }
  & > h4 {
    flex: 1 1 50%;
  }
`;

const StyledContainer = styled(Container)`
  max-width: 1200px;
`;

const Box = styled.div`
  @media (max-width: 550px) {
    flex-direction: column;
  }
`;

const Posts = (props) => {
  const { disposition } = props;
  const [apiP, setApiP] = useState([]);
  const [offset, setOffset] = useState(1);
  const [filters, setFilters] = useState([]);
  const [mount, setMount] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [count, setCount] = useState(0);
  const [search, setSearch] = useState([]);

  const fetchMoreData = () => {
    if (count === apiP.length) {
      setHasMore(false);
    }
    setOffset(offset + 1);
  };

  const getChildData = (data) => {
    setSearch(data.search);
    setHasMore(data.hasMore);
  };

  useEffect(() => {
    getRequest({ request: "posts", query: { offset: offset } })
      .then((result) => {
        setApiP(result.data.data);
        setMount(true);
      })
      .catch((err) => {
        console.error(err);
      });
    getRequest({ request: "posts/count" })
      .then((result) => {
        setCount(result.data);
      })
      .catch((err) => {
        console.error(err);
      });
  }, [offset]);

  let { filter } = useParams();

  const addFilter = (filter) => {
    if (filters.includes(filter)) {
      return;
    }
    setFilters((filters) => [...filters, filter]);
  };

  const removeFilter = (filter) => {
    setFilters(filters.filter((item) => item !== filter));
  };
  const removeAll = () => {
    setFilters([]);
  };
  if (filter) {
    addFilter(filter);
  }
  if (!apiP) {
  }

  return (
    <>
      <StyledContainer>
        <StyledTitle>
          <h1 className="text-center p-1 mt-3 d-none">Blog</h1>
        </StyledTitle>
        <Box className="d-flex">
          <Filter
            filters={filters}
            add={addFilter}
            removeAll={removeAll}
            remove={removeFilter}
          ></Filter>
          <div className="d-flex flex-column">
            <StyledTitle className="d-flex flex-wrap">
              <SearchInput parentCallback={getChildData}></SearchInput>{" "}
              <div className="w-50"></div>
            </StyledTitle>
            <StyledRow>
              {!mount ? (
                <LoadingComponant />
              ) : (
                <InfiniteScroll
                  style={{ overflow: "inherit" }}
                  className="row"
                  dataLength={
                    !search
                      ? 0
                      : search.length > 0
                      ? search.length
                      : apiP.length
                  }
                  next={fetchMoreData}
                  hasMore={hasMore}
                  loader={<LoadingComponant />}
                  scrollThreshold={0.9}
                >
                  {!search ? <EmptyPost>Aucun post trouvé</EmptyPost> : ""}
                  {(!search
                    ? []
                    : search.length > 0
                    ? search.filter((post) => {
                        return filters.every((tag) => post.tags.includes(tag));
                      })
                    : apiP.filter((post) => {
                        return filters.every((tag) => post.tags.includes(tag));
                      })
                  ).map((post, index) => {
                    return (
                      <StyledCol key={index} md={disposition}>
                        <Post
                          postHeight={disposition === 12 ? "200" : 400}
                          tags={post.tags.map((tag) => {
                            return tag;
                          })}
                          detail={post}
                        />
                      </StyledCol>
                    );
                  })}
                </InfiniteScroll>
              )}
            </StyledRow>
          </div>
        </Box>
      </StyledContainer>
    </>
  );
};

export default Posts;
