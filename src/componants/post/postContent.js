import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import Table from "react-bootstrap/Table";
import Interweave from "interweave";

const StyledVideo = styled.div`
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px;
  height: 0;
  overflow: hidden;
  & iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;

const ResponsiveImage = styled.img`
  width: 100%;
  border-radius: 20px;
  height: auto;
  max-width: 600px;
  margin: 0 auto;
`;

const Title = styled.p`
  color: #446cfff7;
  font-weight: bold;
`;

const Paragraph = styled.p`
  font-weight: 300;
`;

const PostContent = (props) => {
  const [blocksd, setBlocks] = useState();
  const { children } = props;
  const data = children;

  const blockFormat = (dataBlock, index) => {
    const { data } = dataBlock;
    switch (dataBlock.type) {
      case "paragraph":
        return (
          <div key={index} className="mt-4 mb-4 ml-0">
            <Paragraph className="text-justify">
              <Interweave content={data.text} />
            </Paragraph>
          </div>
        );
      case "header":
        return (
          <div key={index} className="mt-4 mb-4 ml-0">
            <Title className={`h${data.level} font-weight-bold`}>
              <Interweave content={data.text} />
            </Title>
          </div>
        );

      case "list":
        switch (data.style) {
          case "unordered":
            return (
              <ul key={index} className="mt-4 mb-4 ml-3">
                {data.items.map((item, i) => {
                  return (
                    <li key={i}>
                      <Interweave content={item} />
                    </li>
                  );
                })}
              </ul>
            );
          case "ordered":
            return (
              <ol key={index} className="mt-4 mb-4 ml-3">
                {data.items.map((item, i) => {
                  return (
                    <li key={i}>
                      <Interweave content={item} />
                    </li>
                  );
                })}
              </ol>
            );
          default: {
            return null;
          }
        }
      case "embed":
        return (
          <StyledVideo key={index} className="text-center mt-4 mb-4 ml-0">
            <iframe
              key={index}
              title={data.caption}
              height={data.height}
              width={data.width}
              src={data.embed}
              frameBorder="0"
              allowFullScreen
              referrerPolicy="strict-origin"
            ></iframe>
          </StyledVideo>
        );

      case "image":
        return (
          <div key={index} className="mt-4 mb-4 ml-0 d-flex flex-column">
            <ResponsiveImage
              src={data.file ? data.file.url : ""}
              alt={data.caption}
            />
          </div>
        );

      case "table": {
        return (
          <Table bordered hover key={index} className="mt-4 mb-4 ml-0">
            <tbody>
              {data.content.map((content, index) => {
                return (
                  <tr key={index}>
                    {content.map((data, index) => {
                      return <td key={index}>{data}</td>;
                    })}
                  </tr>
                );
              })}
            </tbody>
          </Table>
        );
      }

      default:
        return "pas encore fait, attend un peu si te plé";
    }
  };

  useEffect(() => {
    if (data) {
      const blocks = children.map((block, index) => {
        return blockFormat(block, index);
      });
      setBlocks(blocks);
    }
  }, [children, data]);

  return <div>{blocksd}</div>;
};

export default PostContent;
