import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import styled from "@emotion/styled";
import Container from "react-bootstrap/Container";
import HeaderPost from "./HeaderPost";
import Badge from "react-bootstrap/Badge";
import { ReactComponent as Logo } from "../svg/turtle.svg";
import { ReactComponent as ColorLogo } from "../svg/colored-turtle.svg";
import { getRequest } from "../../api/axios";
import { postTurtle } from "../../api/services/post.service";
import PostContent from "./postContent";
import { PostLine } from "../../utils/utils";
import { useWindowDimensions } from "../home/main";

const StyledContainer = styled(Container)`
  padding: 25px 10px 500px 10px;
  background-color: white;
  @media (max-width: 500px) {
    padding: 100px 5px 300px 5px;
  }
  @media (max-width: 700px) {
  }
`;

const Content = styled.div`
  padding: 35px;
  & > h1 {
    text-transform: capitalize;
    font-weight: bold;
  }
  width: 100%;
  @media (max-width: 400px) {
    font-size: 4vw;
  }
`;

const StyledSvg = styled.div`
  width: 50px;
  padding: 7px;
  cursor: pointer;
  margin: 0 auto;
  border-radius: 50%;

  &:hover {
    background: #0080003d;
  }
`;

const StyledBar = styled.div`
  opacity: ${({ scrollPos }) => (scrollPos ? (scrollPos - 100) / 100 : "0")};
  padding: 20px;
  position: sticky;
  top: 20%;
  left: 5%;
  height: auto;
  & > div {
    position: sticky;
    top: 100px;
  }
`;

const Count = styled.div`
  user-select: none;
`;

export const StyledBadge = styled(Badge)`
  padding: 8px;
  color: white;
  font-size: 0.8em;
  text-transform: capitalize;
  border: 2px solid #446cfff7;
  transition: all 0.2s ease;
  background: #446cfff7;
  &:hover {
    color: #446cfff7;
    background: white;
  }
  & a {
    color: white;
  }
  &:hover a {
    color: #446cfff7 !important;
    text-decoration: none;
  }
`;

const StyledColorLogo = styled(ColorLogo)`
  display: ${({ clicked }) => (clicked ? "inline-block" : "none")};
`;
const StyledLogo = styled(Logo)`
  display: ${({ clicked }) => (clicked ? "none" : "inline-block")};
`;

const DetailPost = () => {
  const { width } = useWindowDimensions();
  const [postApi, setPost] = useState([]);
  const [like, setLike] = useState(0);
  const [scroll, setScroll] = useState(0);
  const [clicked, setClicked] = useState(false);
  const [tags, setTags] = useState([]);
  const date = new Date(postApi.date);
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };

  const { id } = useParams();

  useEffect(() => {
    getRequest({ request: `posts/${id}` })
      .then((res) => {
        setPost(res.data[0]);
        setTags(res.data[0].tags);
        setLike(res.data[0].turtle);
        setClicked(res.data[0].turtled);
      })
      .catch((err) => console.log(err));
    window.addEventListener("scroll", () => {
      setScroll(window.pageYOffset);
    });
  }, [id]);

  const addLike = () => {
    if (clicked) {
      setClicked(!clicked);
      setLike(like - 1);
      return postTurtle({
        id: id,
        data: { turtled: false, like: like - 1 },
      });
    } else {
      setClicked(!clicked);
      setLike(like + 1);
      return postTurtle({
        id: id,
        data: { turtled: true, like: like + 1 },
      });
    }
  };

  return (
    <>
      <HeaderPost
        img={postApi.image}
        metadata={{
          titre: postApi.titre,
          date: date.toLocaleDateString(undefined, dateOptions),
          author: "Moi",
        }}
      ></HeaderPost>
      <div className="d-flex flex-column">
        <StyledBar scrollPos={scroll} className="col d-none">
          <div>
            <StyledSvg
              id="anim"
              className={`${clicked ? "active" : ""}`}
              onClick={() => {
                addLike();
              }}
            >
              <span className="shape">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </span>
              <StyledLogo clicked={clicked ? 1 : 0} />
              <StyledColorLogo clicked={clicked ? 1 : 0} />
            </StyledSvg>
            <Count>
              <h5 className="text-center mt-2">{like}</h5>
            </Count>
          </div>
        </StyledBar>

        <StyledContainer className={width < 750 ? `col-12` : `col-7`}>
          <p className="text-center text-secondary">
            {tags.map((tag) => {
              return (
                <StyledBadge key={tag} className="m-1" pill variant="secondary">
                  <Link to={`/blog/${tag}`}>{tag}</Link>
                </StyledBadge>
              );
            })}
          </p>
          <div className="d-flex position-relative">
            <Content>
              <PostLine />
              <PostContent>{postApi.content}</PostContent>
            </Content>
          </div>
        </StyledContainer>
        <div className="col"></div>
      </div>
    </>
  );
};

export default DetailPost;
