import React, { useContext, useEffect } from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "../svg/colored-turtle.svg";
import { SwitchContext } from "../header/switchContext";
import Button from "react-bootstrap/Button";
import { deletePostApi } from "../../api/services/post.service";

const Filter = styled.div`
  transition: all 0.8s ease;
  height: 100%;
  width: 100%;
  position: relative;
  background: linear-gradient(#67676733, #000000bd);
  &:hover {
    transition: all 0.5s;
    transition-timing-function: cubic-bezier(0.14, 0.92, 0.73, 1.08);
    background-color: #00000073;
  }
`;

const StyledImage = styled.img`
  object-fit: cover;
  max-width: 100%;
  z-index: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  transition: all 1.5s;
  transition-timing-function: cubic-bezier(0.14, 0.92, 0.73, 1.08);
`;

export const StyledPost = styled.article`
  box-shadow: 0px 0px 30px 20px #94949459;
  border-radius: 20px;
  margin: 30px 0 0 0;
  height: ${({ height }) => height}px;
  width: 100%;
  overflow: hidden;
  &:hover > img {
    transition: all 1.2s;
    transition-timing-function: cubic-bezier(0.14, 0.92, 0.73, 1.08);
    transform: scale(1.15);
  }
`;

const StyledDate = styled.div`
  margin: 20px;
  color: #dfdfdf;
`;

const ContainerPost = styled.div`
  display: flex;
  flex: 1 1 0;
  height: 90%;
  width: 100%;
  position: absolute;
  align-items: flex-end;
`;
const StyledBadge = styled.span`
  background: linear-gradient(192deg, #4db4fd, #63ead8);
  width: fit-content;
  border: none;
  padding: 0 5px 0 5px;
  color: white;
  border-radius: 10px;
  text-transform: capitalize;
  opacity: 1;
  font-size: 0.9rem;
  box-shadow: 0px 2px 9px 6px #4a4a4a2b;
  max-height: 20px;
`;

const StyledTag = styled.div`
  display: flex;
  flex: 1 1 0;
  flex-wrap: wrap;
  width: 75%;
  padding: 7px;
  & > span {
    margin: 3px;
  }
`;

export const StyledLink = styled(Link)`
  color: white;
  &:hover {
    color: white;
    text-decoration: none;
  }
`;

const StyledCount = styled.span`
  font-size: 0.9rem;
  position: absolute;
  top: 4px;
  left: 27px;
`;

const Post = (props) => {
  const { deleteMode } = useContext(SwitchContext);
  const { tags, detail, postHeight, className } = props;
  const date = new Date(detail.date);
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  useEffect(() => {});
  const deletePost = (postId) => {
    deletePostApi({
      query: `posts/delete/${postId}`,
      header: { "x-access-token": "token" },
    })
      .then((response) => {
        // voir pour recuperer chez le parent
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <StyledPost
      height={postHeight}
      className={`position-relative ${className}`}
    >
      <StyledImage src={detail.image} alt="img blog" />
      <Filter>
        <ContainerPost>
          <div className="w-100">
            <StyledDate>
              {date.toLocaleDateString(undefined, dateOptions)}
            </StyledDate>
            <div className="d-flex flex-wrap justify-content-between">
              <h4 className="m-2 text-white font-weight-bold">
                <StyledLink to={`/blog/post/${detail._id}`}>
                  {detail.titre}
                </StyledLink>
              </h4>
              {deleteMode ? (
                <div className="m-2">
                  <Button
                    variant="danger"
                    className="rounded"
                    onClick={() => deletePost(detail._id)}
                  >
                    Supprimer
                  </Button>
                </div>
              ) : null}
            </div>
          </div>
        </ContainerPost>
        <div className="d-flex">
          <StyledTag>
            {tags.map((tag, index) => {
              return <StyledBadge key={index}>{tag}</StyledBadge>;
            })}
          </StyledTag>
          <div className="likes w-25 mt-2">
            <div className="svg position-relative">
              <Logo style={{ width: "30%", maxWidth: "25px" }}></Logo>
              <StyledCount className="text-white">{detail.turtle}</StyledCount>
            </div>
          </div>
        </div>
      </Filter>
    </StyledPost>
  );
};

export default Post;
