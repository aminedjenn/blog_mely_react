import React from "react";
import styled from "@emotion/styled";
import { tagsDetail } from "../../utils/utils";
import { TiDeleteOutline } from "react-icons/ti";
import { Link } from "react-router-dom";
import { StyledBadge } from "./detailPost";

export const StyledFilter = styled.div`
  margin: 0 30px 0 30px;
  min-width: 300px;
  width: 25%;
  padding: 30px;
  border-radius: 22px;
  background: white;
  box-shadow: 0 0 40px 0px #a9a8a84d;
  & div p:hover {
    background-color: #109bfd99;
    cursor: pointer;
    border-radius: 5px;
    color: white;
  }
  & div p {
    text-transform: capitalize;
  }
  & > div {
    top: 50px;
  }
  @media (max-width: 550px) {
    margin: 0;
    width: 100%;
    position: static;
    top: 55px;
    height: fit-content;
    z-index: 10;
    padding: 14px;
    & * {
      padding: 2px !important;
    }
  }
`;

const StyledLink = styled(Link)`
  color: black;
  &:hover {
    text-decoration: none;
  }
  @media (max-width: 550px) {
  }
`;
const RespP = styled.p`
  display: flex;
`;

const StyledButton = styled.span`
  font-size: 1.3em;
  cursor: pointer;
`;

const BadgeFilter = styled(StyledBadge)`
  cursor: auto;
`;

const AllTags = styled.div`
  @media (max-width: 550px) {
    display: flex;
    flex-wrap: wrap;
  }
`;

const Filter = (props) => {
  const { filters } = props;
  const { add, remove, removeAll } = props;

  return (
    <>
      <StyledFilter>
        <div className="position-sticky">
          <div className="mb-1 font-weight-bold">
            <h4>Categories</h4>
            <div className="pt-2">
              <StyledLink to="/blog">
                <span
                  className="mb-0 p-2"
                  onClick={() => {
                    removeAll();
                  }}
                >
                  Tout
                </span>
              </StyledLink>
            </div>
          </div>
          <AllTags className="pb-3 mb-3 border-bottom border-secondary">
            {Object.entries(tagsDetail).map((tag, index) => {
              return (
                <div key={index}>
                  <RespP
                    key={index}
                    onClick={() => add(tag[1].text)}
                    className="mb-0 p-2"
                  >
                    {tag[1].text}
                  </RespP>
                </div>
              );
            })}
          </AllTags>
          {filters.map((filter) => {
            return (
              <BadgeFilter
                key={filter}
                pill
                className="m-1"
                variant="secondary"
              >
                {filter}
                <StyledButton onClick={() => remove(filter)}>
                  <TiDeleteOutline />
                </StyledButton>
              </BadgeFilter>
            );
          })}
        </div>
      </StyledFilter>
    </>
  );
};

export default Filter;
