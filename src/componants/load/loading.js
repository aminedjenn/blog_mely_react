import React from "react";
import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  justify-content: center;
`;

const Content = styled.div`
  position: absolute;
  background-color: #cacaca;
  overflow: hidden;
  left: 7%;
`;

const Title = styled(Content)`
  top: 80%;
  width: 55%;
  height: 30px;
  right: 10%;
  bottom: 0;
  border-radius: 10px;
`;

const Date = styled(Content)`
  height: 15px;
  top: 71%;
  width: 70%;
  border-radius: 10px;
`;

const Tags = styled(Content)`
  width: 50%;
  height: 20px;
  top: 5%;
  border-radius: 10px;
`;

const Turtle = styled(Content)`
  width: 30px;
  height: 30px;
  border-radius: 20px;
  left: 80%;
  top: 3%;
`;

const Loading = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  max-width: 1200px;

  & div.block {
    position: relative;
    flex: 1 1 25%;
    position: relative;
    height: 400px;
    margin: 20px;
    border-radius: 30px;
    background-color: rgb(224, 224, 224);
    transition: all 1s;
    overflow: hidden;
    min-width: 200px;
  }
  & div::after {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    transform: translateX(-100%);
    animation: 2s load ease infinite;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0) 30%,
      rgba(255, 255, 255, 0.5) 50%,
      rgba(255, 255, 255, 0.5) 60%,
      rgba(238, 238, 238, 0) 80%
    );
    content: "";
  }
`;

const LoadingComponant = () => {
  return (
    <Container>
      <Loading>
        {[0, 1, 2, 3, 4, 5].map((index) => {
          return (
            <div className="block" key={index}>
              <Tags />
              <Turtle />
              <Date />
              <Title />
            </div>
          );
        })}
      </Loading>
    </Container>
  );
};

export default LoadingComponant;
