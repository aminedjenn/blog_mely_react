import React from "react";
import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
`;

const Content = styled.div`
  position: absolute;
  background-color: #cacaca;
  overflow: hidden;
  left: 6%;
`;

const Title = styled(Content)`
  top: 82%;
  width: 50%;
  height: 24px;
  right: 10%;
  bottom: 0;
  border-radius: 20px;
`;

const Date = styled(Content)`
  height: 15px;
  top: 69%;
  width: 60%;
  border-radius: 20px;
`;

const Tags = styled(Content)`
  width: 50%;
  height: 20px;
  top: 8%;
  border-radius: 20px;
`;

const Turtle = styled(Content)`
  width: 30px;
  height: 30px;
  border-radius: 20px;
  left: 80%;
  top: 6%;
`;

const Loading = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  flex: 1 1 0;
  max-width: 1100px;
  & div.block:nth-of-type(2) {
    margin-left: 0;
  }
  & div.block:nth-of-type(3) {
    margin-right: 0;
    margin-left: 0;
  }

  & div.block {
    position: relative;
    flex: 1 1 25%;
    height: 200px;
    margin: 15px;
    border-radius: 30px;
    background-color: rgb(224, 224, 224);
    transition: all 1s;
    overflow: hidden;
    min-width: 200px;
  }
  & div.big-block {
    position: relative;
    flex: 1 1 100%;
    height: 500px;
    margin: 0;
    border-radius: 30px;
    background-color: rgb(224, 224, 224);
    transition: all 1s;
    overflow: hidden;
    min-width: 200px;
  }

  & div::after {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    transform: translateX(-100%);
    animation: 2s load ease infinite;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0) 30%,
      rgba(255, 255, 255, 0.5) 50%,
      rgba(255, 255, 255, 0.5) 60%,
      rgba(238, 238, 238, 0) 80%
    );
    content: "";
  }
`;

const HomeLoadingComponant = () => {
  return (
    <Container>
      <Loading>
        <div className="big-block">
          <Tags />
          <Turtle />
          <Date />
          <Title />
        </div>{" "}
        <div className="block">
          <Tags />
          <Turtle />
          <Date />
          <Title />
        </div>{" "}
        <div className="block">
          <Tags />
          <Turtle />
          <Date />
          <Title />
        </div>
      </Loading>
    </Container>
  );
};

export default HomeLoadingComponant;
