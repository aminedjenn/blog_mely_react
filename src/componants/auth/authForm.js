import React, { useState, useContext } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import styled from "@emotion/styled";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { loginBack } from "../../api/services/auth.service";
import storeUser from "../../middleware/storeUser.js";
import { CustomAlert } from "../../utils/utils.js";
import { AuthContext } from "./authContext";
import Cookies from "universal-cookie";

const ContainerForm = styled(Form)`
  box-shadow: 0px 3px 9px -2px #9090909e;
  background: white;
  max-width: 450px;
  min-width: 300px;
  border-radius: 7px;
  & > button {
    background: rgb(84 105 212);
    font-weight: 600;
    letter-spacing: 0.5px;
    border-color: rgb(84 105 212);
  }
  & > button:hover, & > button:focus, & > button:active {
    background: rgb(139 149 204)!important;
    border: 1px solid rgb(139 149 204)!important;
  }
  @media (max-width: 460px) {
    min-width: 300px;
    width: 100%!important;
}
  }
`;
const Label = styled(Form.Label)`
  font-size: 0.9em;
  font-weight: 600;
`;

const FormGroup = styled(Form.Group)`
  margin-bottom: 25px;
  position: relative;
`;

export const Input = styled.input`
  position: relative;
  padding: 8px 16px;
  width: 100%;
  border-radius: 4px;
  min-height: 47px;
  border: 1px solid #a9a9a963;
  &:focus-visible,
  &:focus {
    outline-color: #9797ff;
  }
`;

const Container = styled.div`
  position: relative;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const AuthForm = (props) => {
  const [view, setView] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const cookies = new Cookies();
  const contextValue = useContext(AuthContext);

  const login = (e) => {
    e.preventDefault();
    const { email, password } = e.target;
    const response = loginBack({
      request: "user/login",
      data: { email: email.value, password: password.value },
    });
    response
      .then((result) => {
        storeUser(result.data);
        contextValue.setToken(cookies.get("token"));
        contextValue.setIsLoggedIn(cookies.get("token") ? true : false);
        contextValue.login(result.data.user);
      })
      .catch((err) => {
        setShowError(true);
        setErrorMessage(err.response.data.message);
      });
  };
  const { children } = props;
  return (
    <>
      <div style={{ height: "100%" }}>
        <Container className="w-auto">
          <div className="blob-form">
            <svg
              className="blob"
              xmlns="http://www.w3.org/2000/svg"
              x="0px"
              y="0px"
              viewBox="0 0 200 200"
            >
              <defs>
                <linearGradient id="grad1" x1="0%" y1="0%" x2="50%" y2="100%">
                  <stop
                    offset="0%"
                    style={{ stopColor: "rgb(124, 141, 240)", stopOpacity: 1 }}
                  />
                  <stop
                    offset="100%"
                    style={{ stopColor: "rgb(34, 3, 175)", stopOpacity: 1 }}
                  />
                </linearGradient>
              </defs>
              <path fill="url(#grad1)" transform="translate(100 100)" />
            </svg>
          </div>
          <ContainerForm onSubmit={login} className="p-5 w-50">
            <CustomAlert
              message={errorMessage}
              show={showError}
              setShow={setShowError}
            ></CustomAlert>
            <h4 className="mb-4">Connexion</h4>
            <FormGroup>
              <Label htmlFor="formBasicEmail">Email</Label>
              <Input id="formBasicEmail" type="email" name="email" />
            </FormGroup>

            <FormGroup>
              <Label htmlFor="formBasicPassword">Mot de passe</Label>
              <Input
                id="formBasicPassword"
                type={`${view ? "text" : "password"}`}
                name="password"
              />
              <i
                style={{
                  cursor: "pointer",
                  position: "absolute",
                  top: "42px",
                  right: "39px",
                }}
                onClick={() => {
                  setView(!view);
                }}
              >
                {view ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
              </i>
            </FormGroup>
            <FormGroup>
              <Form.Check type="checkbox" label="Se souvenir de moi" />
            </FormGroup>
            <Button variant="primary" className="btn-block p-2" type="submit">
              Valider
            </Button>
          </ContainerForm>
          {children}
        </Container>
      </div>
    </>
  );
};

export default AuthForm;
