import React, { useContext, useEffect } from "react";
import { AuthContext } from "./authContext";
import { SwitchContext } from "../header/switchContext";
import Cookies from "universal-cookie";

const Logout = () => {
  const cookies = new Cookies();
  const { setIsLoggedIn, login, setToken } = useContext(AuthContext);
  const { setDeleteMode } = useContext(SwitchContext);
  useEffect(() => {
    localStorage.removeItem("isLogin");

    setDeleteMode(false);
    setIsLoggedIn(false);
    login(null);
    setToken(null);

    cookies.remove("token");
  });

  return null;
};

export default Logout;
