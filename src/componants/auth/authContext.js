import { createContext } from "react";

export const AuthContext = createContext({
  isLoggedIn: null,
  user: null,
  token: null,
  login: () => {},
  setToken: () => {},
  setIsLoggedIn: () => {},
});
