import Header from "@editorjs/header";
import Embed from "@editorjs/embed";
import ImageTool from "@editorjs/image";
import List from "@editorjs/list";
import Table from "@editorjs/table";
import Checklist from "@editorjs/checklist";
import Underline from "@editorjs/underline";

export const tools = {
  header: {
    class: Header,
    levels: [1, 2, 3, 6],
    defaultLevel: 2,
    inlineToolbar: ["link", "bold"],
  },
  underline: Underline,
  embed: {
    class: Embed,
    config: {
      services: {
        youtube: true,
        coub: true,
      },
    },
  },
  checklist: {
    class: Checklist,
    inlineToolbar: true,
  },
  table: {
    class: Table,
    inlineToolbar: true,
    config: {
      rows: 2,
      cols: 3,
    },
  },
  image: {
    class: ImageTool,
    inlineToolbar: true,
    config: {
      placeholder: "Click pour choisir l'image",
      endpoints: {
        byFile: "https://blob-api.herokuapp.com/api/file/uploadImagePost",
        //byUrl: "https://api.cloudinary.com/v1_1/dvbzs4nlk/image/upload",
      },
      field: "file",
      captionPlaceholder: "Description",
    },
  },
  list: {
    class: List,
    inlineToolbar: true,
  },
};
