import React, { useState } from "react";
import styled from "@emotion/styled";
import { Input } from "../auth/authForm";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";

const InputCategories = styled(Input)`
  font-size: 0.9em;
  height: 49px;
  width: 100%;
  background: #f7f7f7;
  &:not(:focus) ~ div {
    display: none;
  }
`;

const CategoriesContainer = styled.div`
  padding: 13px;
  background: white;
  width: 312px;
  height: fit-content;
  position: relative;
  box-shadow: 0px 3px 9px 0px #c5c5c554;
  border-radius: 20px;
  margin: 0 0 0 30px;
  border: 1px solid rgb(239 239 239);
`;

const Help = styled.div`
  width: 92%;
  display: ${({ display }) => display};
  height: 69px;
  position: absolute;
  bottom: -53px;
  background: white;
  border: 1px solid #9696969c;
  border-radius: 0px 0px 10px 10px;
  color: #292929;
  padding: 10px;
  font-size: 0.9em;
`;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    listStyle: "none",
    padding: theme.spacing(0.5),
    margin: 0,
  },
  chip: {
    margin: theme.spacing(0.5),
  },
}));

const InputComponant = (props) => {
  const classes = useStyles();
  const [tags, setTags] = useState([]);
  const [index, setIndex] = useState(0);
  const [hidden, setHidden] = useState(true);
  const [value, setValue] = useState("");

  const handleDelete = (chipToDelete) => () => {
    setTags((chips) => chips.filter((chip) => chip.key !== chipToDelete.key));
  };
  const handleTyping = (e) => {
    const value = e.target.value;
    if (e.key === "Enter" && value.length > 0) {
      console.log(index);
      setTags((chips) => [...chips, { key: index, label: value }]);
      console.log(tags);
      setIndex(index + 1);
      e.target.value = "";
      props.getTags({ tags: tags });
    }
    if (!e.target.value || e.target.value.length > 20) {
      setHidden(true);
      return;
    }
    setValue(value);
    setHidden(false);
  };

  return (
    <CategoriesContainer>
      <h4>Catégories</h4>
      {tags.map((data) => {
        return (
          <Chip
            key={data.key}
            label={data.label}
            onDelete={handleDelete(data)}
            className={classes.chip}
            color={data.color}
          />
        );
      })}
      <hr /> <InputCategories type="text" onKeyUp={handleTyping} />
      <Help display={`${hidden ? "none" : "block"}`}>
        Pour valider la catégorie
        <span className="font-weight-bold">"{value}"</span>, appuies sur Entrer
      </Help>
    </CategoriesContainer>
  );
};

export default InputComponant;
