import React, { useState } from "react";
import styled from "@emotion/styled";

const FileInput = styled.input`
  opacity: 0.1;
  position: absolute;
`;

const StyledFileUploader = styled.div`
  & label {
    display: block;
    position: relative;
    height: 42px;
    border-radius: 0px;
    background: white;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #9e9e9e;
    cursor: pointer;
    transition: all 0.2s ease-out;
    padding: 50px;
    border: 2px dashed;
  }
  & label:hover {
    color: #797979;
    background-color: #e0e0e0;
    border-color: #000000;
  }
  & p {
    font-style: italic;
    margin-bottom: 0;
    border-bottom: 1px solid #ababab;
    padding: 4px;
  }
`;

const FileUploader = ({ onFileSelectSuccess, onFileSelectError }) => {
  const [display, setDisplay] = useState("d-none");
  const [fileName, setFileName] = useState("");
  const handleFileInput = (e) => {
    const file = e.target.files[0];
    setDisplay("d-block");
    if (!file) {
      return console.log("annulé");
    }
    setFileName(file.name ? file.name : "");
    onFileSelectSuccess(file);
  };

  return (
    <StyledFileUploader className="file-uploader mt-2">
      <FileInput
        accept="image/*"
        type="file"
        required={true}
        id="file"
        onChange={handleFileInput}
      />
      <label
        className="btn-secondary"
        onClick={() => setDisplay("d-block")}
        onMouseLeave={() => setDisplay("d-none")}
        htmlFor="file"
      >
        <div
          className={`spinner-border text-dark mr-2 ${display}`}
          role="status"
        ></div>
        Image de présentation
      </label>
      <p className="FileName">{fileName ? fileName : "Aucun fichier choisi"}</p>
    </StyledFileUploader>
  );
};

export default FileUploader;
