import React, { useCallback, useRef, useState, useContext } from "react";
import EditorJs from "react-editor-js";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import styled from "@emotion/styled";
import { tagsDetail, MyVerticallyCenteredModal } from "../../utils/utils";
import Select from "react-select";
import { postRequest } from "../../api/axios";
import FileUploader from "./fileComponant";
import { tools } from "./tools";
import { ImCheckmark, ImCross } from "react-icons/im";
import { AuthContext } from "../auth/authContext";
import InputComponant from "./inputComponant";

const StyledAdd = styled.div`
  background-color: white;
  padding: 20px;
  border-radius: 20px;
  position: relative;
  border: 1px solid rgb(239 239 239);
  box-shadow: 0px 3px 12px 2px #d2d2d254;
  & div#editor div {
    z-index: 0 !important;
  }
  @media (max-width: 650px) {
    margin: 10% 0;
  } ;
`;

const ButtonBottom = styled(Button)`
  z-index: 999;
  padding: 0.8em 1.5em;
  min-width: max-content;
  border-radius: 10px !important;
  font-weight: bolder;
  border: 1px solid #dee2e64a !important;
  position: absolute;
  bottom: -20px;
  transition: all 0.2s ease-in-out;
`;

const SpanCheck = styled.span`
  padding: 17px 20px;
  border-radius: 50px;
  background: ${({ color }) => color};
  margin-left: 12px;
`;

const StyledForm = styled(Form)`
  & div input[name="titre"] {
    font-size: 2em;
    border: none;
    font-weight: bold;
  }
`;

const StyledEditor = styled(EditorJs)`
  z-index: 0;
  flex: 1 1 0;
  max-width: 600px;
  min-width: 350px;
  margin-left: 20px;
`;

const ContainerEditor = styled.div`
  flex-wrap: wrap;
  display: flex;
  justify-content: center;
  margin: 50px 0 0 0;
`;

const AddPost = (props) => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [modalShow, setModalShow] = useState(false);
  const [message, setMessage] = useState("");
  const [childTags, setChildTags] = useState([]);
  const [selectedOption, setSelectedOption] = useState([]);
  const { token } = useContext(AuthContext);
  const options = Object.entries(tagsDetail).map((tag) => {
    return { value: tag[1].text, label: tag[1].text };
  });

  const getChildTags = (data) => {
    setChildTags(data.tags);
  };

  const instanceEditor = useRef(null);
  const onSave = useCallback(async () => {
    const outputData = await instanceEditor.current.save();
    if (outputData.blocks.length <= 0) {
      return new Error("Essaie de tout remplir please");
    }
    return outputData;
  }, []);

  const validation = (formData) => {
    const tags = formData.get("tags").split(",");
    const body = JSON.parse(formData.get("body").trim());
    let valid;
    return valid = (tags.length > 0 && Object.keys(tags).length > 0 && tags[0].length > 0) ||
      body.length > 0
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onSave()
      .then((res) => {
        const tags = childTags.map((option) => {
          return option.label;
        });
        console.log(tags);
        const formData = new FormData();
        formData.append("file", selectedFile);
        formData.append("title", event.target.titre.value);
        formData.append("tags", tags);
        formData.append("body", JSON.stringify(res));
        const v = validation(formData);
        if (res instanceof Error || !v) {
          setMessage(
            <div>
              <h3>
                {res.message ||
                  "Erreur, essaie de mettre une catégorie au moin"}
              </h3>
              <SpanCheck color="red">
                <ImCross fill="white" />
              </SpanCheck>
            </div>
          );
          setModalShow(true);
        } else {
          postRequest({
            request: "posts/add",
            data: formData,
            blocks: res,
            headers: { "x-access-token": token },
          })
            .then(() => {
              setModalShow(true);
              setMessage(
                <div>
                  <h3>Article sauvegardé</h3>
                  <SpanCheck color="green">
                    <ImCheckmark fill="white" />
                  </SpanCheck>
                </div>
              );
            })
            .catch((err) => {
              setMessage(err);
              setModalShow(true);
              console.error(err);
            });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const headers = new Headers();
  headers.append("Access-Control-Allow-Origin", "*");
  headers.append("Access-Control-Allow-Methods", "*");
  headers.append("Content-Type", "image/jpeg");

  return (
    <>
      <StyledEditor
        instanceRef={(instance) => (instanceEditor.current = instance)}
        holder="editor"
        placeholder="C'est parti pour un poste super coule ! tu peux ecrire le contenu ici"
        onReady={() => {
          console.log("Prêt");
        }}
        tools={tools}
      >
        <ContainerEditor>
          <StyledAdd style={{ zIndex: 0 }}>
            <StyledForm onSubmit={handleSubmit}>
              <div className="form-group">
                <input
                  required={true}
                  className="form-control"
                  type="text"
                  name="titre"
                  placeholder="Titre"
                />

                <FileUploader
                  onFileSelectSuccess={(file) => setSelectedFile(file)}
                  onFileSelectError={({ error }) => alert(error)}
                />
              </div>
              <div className="form-group d-none">
                <Select
                  required={true}
                  isMulti
                  placeholder="Catégories"
                  name="tags"
                  defaultValue={selectedOption}
                  className="basic-muti-select"
                  onChange={(selected) => {
                    setSelectedOption(selected);
                  }}
                  options={options}
                />
              </div>
              <ButtonBottom className="btn-violet" type="submit">
                Mine j'ai fini 😀
              </ButtonBottom>
              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                content={message}
              />
            </StyledForm>
            <div id="editor" style={{ maxWidth: "480px" }}></div>
          </StyledAdd>
          <InputComponant getTags={getChildTags}></InputComponant>
        </ContainerEditor>
      </StyledEditor>
    </>
  );
};

export default AddPost;
