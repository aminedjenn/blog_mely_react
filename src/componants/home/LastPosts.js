import React, { useEffect, useState } from "react";
import Post from "../post/post";
import HomeLoadingComponant from "../load/homeLoading";
import styled from "@emotion/styled";
import { NoPost } from "../../utils/utils";
import { getLastsPost } from "../../api/services/post.service";
const ContainerLatest = styled.div`
  & article:nth-of-type(1) {
    margin-left: 0;
    margin-right: 15px !important;
  }
  & article:nth-of-type(2) {
    margin-right: 0 !important;
    margin-left: 0;
  }
`;

const LastPosts = (props) => {
  const { children } = props;
  const [last, setLast] = useState();

  useEffect(() => {
    getLastsPost("posts/last")
      .then((posts) => {
        console.log(posts);
        setLast(posts);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  if (!last) {
    return <HomeLoadingComponant />;
  }

  if (last.length <= 0) {
    return <NoPost />;
  }

  return (
    <>
      {children}
      <div className="d-flex flex-column">
        <Post postHeight="500" tags={last[0].tags} detail={last[0]}></Post>
        <ContainerLatest className="d-flex">
          {last.slice(1, 3).map((post, index) => {
            return (
              <Post
                key={index}
                className="mr-2"
                postHeight="200"
                tags={post.tags}
                detail={post}
              ></Post>
            );
          })}
        </ContainerLatest>
      </div>
    </>
  );
};

export default LastPosts;
