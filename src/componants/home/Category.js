import React from "react";
import styled from "@emotion/styled";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
const CategoryContainer = styled.div`
  margin: 50px 0px 0 0;
  @media (max-width: 1100px) {
    padding: 0;
  }
  @media (max-width: 850px) {
    width: 100%;
    margin: 50px 0px 50px 0px;
  }
`;

const StyledCategory = styled.div`
  box-shadow: -14px 20px 50px 6px #6b6b6b29;
  border-radius: 20px;
  position: relative;
  background-color: white;
  width: 400px;
  min-height: 450px;

  @media (max-width: 750px) {
    left: 0;
  }
  @media (max-width: 500px) {
    width: 100%;
    transition: none;
  }
  & > div div:nth-of-type(2) > img {
    border-radius: 0 20px;
  }
  & > div {
    min-height: 150px;
  }
`;

const ButtonStyledDiv = styled.div`
  top: 35%;
  left: 55%;
  @media (max-width: 750px) {
    left: 45%;
  }
  @media (max-width: 400px) {
    left: 10%;
    top: 100%;
  }
`;
const ButtonStyled = styled(Button)`
  white-space: nowrap;
  border-radius: 15px;
  padding: 30px 80px 30px 80px;
  background: ${({ color }) => color.btnColor};
  box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.3) !important;
  transition: all 0.4s;
`;

const StyledText = styled.div``;
const MediaDiv = styled.div`
  @media (max-width: 400px) {
    display: contents;
  }
`;

const Category = (props) => {
  const { placement } = props;
  const { type } = props;
  const propreties = {
    left: {
      paddingLeft: 0,
      paddingRight: "50%",
      backBlock: { left: "40%", right: 0 },
    },

    right: {
      paddingLeft: "40%",
      paddingRight: 0,
      backBlock: { left: "-25%", right: 0 },
    },
  };

  const typePropreties = {
    plante: {
      img: "/img/plante.gif",
      bgColor: "#91C7B1",
      btnColor:
        "linear-gradient(146deg, rgb(17 165 126) 12%, rgb(90 255 162 / 78%) 79%)",
    },
    lune: {
      img: "/img/moon.gif",
      bgColor: "#183059",
      btnColor:
        "linear-gradient(146deg, rgb(18 66 144) 8%, rgb(152 149 251 / 75%) 120%)",
    },
    lithoterapie: {
      img: "/img/pierre.jpg",
      bgColor: "#91C7B1",
      btnColor:
        "linear-gradient(146deg, rgba(145,199,177,1) 12%, rgba(217,255,233,0.7791491596638656) 79%)",
    },
    astrologie: {
      img: "/img/astro.gif",
      bgColor: "#F7C59F",
      btnColor:
        "linear-gradient(138deg, rgb(145 120 253) 12%, rgb(223 179 255 / 91%) 79%)",
    },
  };

  return (
    <>
      <CategoryContainer
        placement={propreties[placement]}
        data-aos="fade-up"
        data-aos-delay="100"
      >
        <StyledCategory>
          <div className="d-flex align-content-center">
            <div className="p-1 w-100 align-self-center">
              <h2 className="text-center text-capitalize m-2">{type}</h2>
            </div>
            <div className="w-100">
              <img
                width="100%"
                height="100%"
                src={typePropreties[type].img}
                alt="plante"
              ></img>
            </div>
          </div>
          <StyledText className="d-flex position-relative h-100">
            <div className="overflow-hidden p-1 w-100">
              <p className="p-3 h-100">
                lorem ipsum dolor sit amet, consectetur adip looreet, sedorem
                ipsum dolor sit amet, consectetur adip looreet, sedorem ipsum
                dolor sit amet, consectetur adip looreet, sed
              </p>
            </div>
            <MediaDiv
              className="p-1
           w-100"
            >
              <ButtonStyledDiv className="position-absolute">
                <Link to={`/blog/${type}`}>
                  <ButtonStyled
                    color={typePropreties[type]}
                    className="shadow-lg btn-dark border-0"
                  >
                    Voir plus
                  </ButtonStyled>
                </Link>
              </ButtonStyledDiv>
            </MediaDiv>
          </StyledText>
        </StyledCategory>
      </CategoryContainer>
    </>
  );
};

export default Category;
