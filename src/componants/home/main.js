import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import { UnderTitle } from "../../utils/utils";
import Category from "./Category";
import LastPosts from "./LastPosts";

const CenteredDiv = styled.div`
  display: flex;
  justify-content: center;
  text-align: ${({ center }) => (center < 700 ? "-webkit-center" : "")};
`;

export const StyledText = styled.h2`
  margin: 30px 0;
  font-size: 3rem;
  background: linear-gradient(
    120deg,
    #446cfff7 12%,
    rgb(227 140 255 / 95%) 36%,
    rgba(255, 79, 165, 1) 76%
  );
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  font-weight: bold;
`;
const ResponsiveContainer = styled.div`
  margin: auto;
  max-width: 1100px;
  min-height : 120%;
  @media (max-width: 900px) {
    padding: 0;
    margin: 0;
    & 
  }
  & div.row {
    max-width:1000px;
  }
`;

export const getWindowDimensions = () => {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
};

export const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );
  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
};

const Main = (props) => {
  const { width } = useWindowDimensions();
  return (
    <ResponsiveContainer
      className={`${width < 700 ? "" : "d-flex flex-column"}`}
    >
      <LastPosts>
        <StyledText>
          Derniers posts
          <UnderTitle />
        </StyledText>
      </LastPosts>
      <StyledText>
        Mes passions
        <UnderTitle />
      </StyledText>
      <CenteredDiv center={width}>
        <div className="row">
          <div className="col-md-6">
            <Category type="plante" placement="left" />
          </div>
          <div className="col-md-6">
            <Category type="astrologie" placement="right" />
          </div>
          <div className="col-md-6">
            <Category type="lithoterapie" placement="left" />
          </div>
          <div className="col-md-6">
            <Category type="lune" placement="right" />
          </div>
        </div>
      </CenteredDiv>
    </ResponsiveContainer>
  );
};

export default Main;
