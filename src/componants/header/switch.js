import React, { useContext } from "react";
import { SwitchContext } from "./switchContext";

const Switch = () => {
  const { deleteMode, setDeleteMode } = useContext(SwitchContext);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {/* <label htmlFor="switch">
        Delete mode{" "}
        <span style={{ textDecoration: "underline" }}>
          {deleteMode ? "ON" : "OFF"}
        </span>
      </label> */}
      <div className="switch r" id="switch-1">
        <input
          id="switch"
          type="checkbox"
          checked={deleteMode}
          onChange={() => {
            setDeleteMode(!deleteMode);
          }}
          className="checkbox"
        />{" "}
        <div className="knobs" />
        <div className="layer" />
      </div>
    </div>
  );
};

export default Switch;
