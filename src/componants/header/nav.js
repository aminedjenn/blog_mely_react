import React from "react";
import Nav from "react-bootstrap/Nav";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const StyledBar = styled(Nav)`
  box-shadow: -15px 0px 5em 6px #2d2d2db5;
  height: 100vh;
  & div > * {
    color: rgba(255, 255, 255, 0.6);
    text-decoration: none;
    transition: all 0.2s ease;
    font-size: 1.1rem;
    border-bottom: 1px solid rgba(255, 255, 255, 0.1);
  }
  & div > a:hover {
    background-color: #9f88d8;
    text-decoration: none;
    color: white;
  }
`;
const CollapseNav = styled.nav`
  top: 0;
  background-color: #32373d;
  position: fixed;
  width: 300px;
  transition: all 0.8s cubic-bezier(0.47, 0.72, 0.4, 0.99);
  opacity: ${({ open }) => (open ? "1" : "0.2")};
  transform: ${({ open }) => (open ? "translateX(0)" : "translateX(-400px)")};
  z-index: 99;
  & body {
    overflow: ${({ open }) => (open ? "hidden" : "auto")};
  }
`;
const Navs = (props) => {
  const { open } = props;
  return (
    <>
      <CollapseNav open={open}>
        <StyledBar className="pt-5">
          <Nav defaultActiveKey="/home" className="flex-column w-100">
            <div className="p-5 w-100">Melythologie.com</div>
            <Link to="/" className="p-4">
              Menu
            </Link>
            <Link to="/blog" className="p-4">
              Blog
            </Link>
          </Nav>
        </StyledBar>
      </CollapseNav>
    </>
  );
};
export default Navs;
