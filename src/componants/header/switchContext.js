import { createContext } from "react";
const defaultValue = {
  deleteMode: null,
  setDeleteMode: () => {},
};

export const SwitchContext = createContext(defaultValue);
