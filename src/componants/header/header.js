import styled from "@emotion/styled";
import Navs from "./nav";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import Switch from "./switch";

const StyledButton = styled.div`
  background-color: black;
  height: 4px;
  width: 2rem;
`;

const StyledCreate = styled(Button)`
  padding: 0.8em 1.5em;
  min-width: max-content;
  border-radius: 10px !important;
  transition: all 0.2s ease-in-out 0s;
  font-weight: bolder;
  border: 1px solid #dee2e64a !important;
  @media (max-width: 500px) {
    margin: auto;
    height: auto;
    min-width: auto;
  }
`;

const FixedHeader = styled.div`
  width: 100%;
  top: 0;
  border-bottom: 1px solid #d8d8d8;
`;

const HeaderStyled = styled.div`
  min-height: 45px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const StyledBurger = styled.div`
  z-index: 999;
  cursor: pointer;
  transition: all 0.5s;
  & > div {
    margin: 6px;
    transition: all 0.3s ease-in-out;
    transform-origin: 6% center;
    border-radius: 5px;
  }
  &:hover > div:nth-of-type(2) {
    width: 2em !important;
  }
  &:active > div:nth-of-type(2) {
    width: 2em !important;
  }

  div:first-of-type {
    transform: ${({ open }) => (open ? "rotate(45deg)" : "rotate(0)")};
  }
  div:last-child {
    transform: ${({ open }) => (open ? "rotate(-45deg)" : "rotate(0)")};
  }
  div:nth-of-type(2) {
    transform: ${({ open }) => (open ? "scale(0)" : "scale(1)")};
    width: 1.4em;
    background: #9f88d8;
  }
  div {
    background-color: ${({ open }) =>
      open ? "rgba(255, 255, 255, 0.6)" : "black"};
  }
`;

const TitleName = styled.h2`
  letter-spacing: -0.07em;
  @media (max-width: 650px) {
    font-size: 5vw;
  }
  @media (max-width: 460px) {
    display: none;
  }
`;

const Header = (props) => {
  const { onClick } = props;
  const { open } = props;
  //const { isLoggedIn } = useContext(AuthContext);
  const login = Boolean(localStorage.getItem("isLogin"));

  const routes = [
    {
      to: "/connexion",
      text: "Connexion",
      needLogin: false,
      class: "",
      variant: "light",
    },
    {
      to: "/post/add",
      text: "Ecrire un post",
      class: "ml-2 btn-violet",
      variant: "primary",
      needLogin: true,
    },
    {
      to: "/logout",
      text: "Déconnexion",
      class: "ml-2",
      variant: "light",
      needLogin: true,
    },
  ];

  return (
    <>
      <FixedHeader className="d-flex justify-content-between bg-white p-3 position-relative">
        <StyledBurger
          open={open}
          onClick={onClick}
          className="position-fixed p-2"
        >
          <StyledButton />
          <StyledButton />
          <StyledButton />
        </StyledBurger>
        <Link to="/" alt="logo" style={{ marginLeft: "80px" }}>
          <img src="/img/mineral.png" alt="logo-mel" width="50" />
        </Link>

        <HeaderStyled>
          <TitleName className="text-center mt-2 ml-2">{props.name}</TitleName>
        </HeaderStyled>

        <div className="d-flex flex-wrap">
          {login ? <Switch /> : null}
          {routes.map((route, index) => {
            if (login === route.needLogin) {
              return (
                <div key={index}>
                  <Link to={route.to}>
                    <StyledCreate
                      variant={route.variant}
                      className={route.class}
                    >
                      {route.text}
                    </StyledCreate>
                  </Link>
                </div>
              );
            } else return null;
          })}
        </div>
      </FixedHeader>
      <Navs open={open}> </Navs>
    </>
  );
};

export default Header;
